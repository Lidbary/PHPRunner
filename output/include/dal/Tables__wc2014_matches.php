<?php
$dalTablewc2014_matches = array();
$dalTablewc2014_matches["ID"] = array("type"=>3,"varname"=>"ID");
$dalTablewc2014_matches["team1"] = array("type"=>200,"varname"=>"team1");
$dalTablewc2014_matches["team2"] = array("type"=>200,"varname"=>"team2");
$dalTablewc2014_matches["datetime"] = array("type"=>135,"varname"=>"datetime");
$dalTablewc2014_matches["match_number"] = array("type"=>3,"varname"=>"match_number");
$dalTablewc2014_matches["location"] = array("type"=>200,"varname"=>"location");
$dalTablewc2014_matches["team1_goals"] = array("type"=>3,"varname"=>"team1_goals");
$dalTablewc2014_matches["team2_goals"] = array("type"=>3,"varname"=>"team2_goals");
$dalTablewc2014_matches["team1_code"] = array("type"=>200,"varname"=>"team1_code");
$dalTablewc2014_matches["team2_code"] = array("type"=>200,"varname"=>"team2_code");
$dalTablewc2014_matches["status"] = array("type"=>200,"varname"=>"status");
	$dalTablewc2014_matches["ID"]["key"]=true;

$dal_info["Tables__wc2014_matches"] = &$dalTablewc2014_matches;
?>