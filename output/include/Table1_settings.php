<?php
require_once(getabspath("classes/cipherer.php"));




$tdataTable1 = array();
	$tdataTable1[".truncateText"] = true;
	$tdataTable1[".NumberOfChars"] = 80;
	$tdataTable1[".ShortName"] = "Table1";
	$tdataTable1[".OwnerID"] = "";
	$tdataTable1[".OriginalTable"] = "Table1";

//	field labels
$fieldLabelsTable1 = array();
$fieldToolTipsTable1 = array();
$pageTitlesTable1 = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsTable1["English"] = array();
	$fieldToolTipsTable1["English"] = array();
	$pageTitlesTable1["English"] = array();
	$fieldLabelsTable1["English"]["ID"] = "ID";
	$fieldToolTipsTable1["English"]["ID"] = "";
	$fieldLabelsTable1["English"]["Field1"] = "Field1";
	$fieldToolTipsTable1["English"]["Field1"] = "";
	if (count($fieldToolTipsTable1["English"]))
		$tdataTable1[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsTable1[""] = array();
	$fieldToolTipsTable1[""] = array();
	$pageTitlesTable1[""] = array();
	if (count($fieldToolTipsTable1[""]))
		$tdataTable1[".isUseToolTips"] = true;
}


	$tdataTable1[".NCSearch"] = true;



$tdataTable1[".shortTableName"] = "Table1";
$tdataTable1[".nSecOptions"] = 0;
$tdataTable1[".recsPerRowList"] = 1;
$tdataTable1[".recsPerRowPrint"] = 1;
$tdataTable1[".mainTableOwnerID"] = "";
$tdataTable1[".moveNext"] = 1;
$tdataTable1[".entityType"] = 0;

$tdataTable1[".strOriginalTableName"] = "Table1";




$tdataTable1[".showAddInPopup"] = false;

$tdataTable1[".showEditInPopup"] = false;

$tdataTable1[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataTable1[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataTable1[".fieldsForRegister"] = array();

$tdataTable1[".listAjax"] = false;

	$tdataTable1[".audit"] = false;

	$tdataTable1[".locking"] = false;

$tdataTable1[".edit"] = true;
$tdataTable1[".afterEditAction"] = 1;
$tdataTable1[".closePopupAfterEdit"] = 1;
$tdataTable1[".afterEditActionDetTable"] = "";

$tdataTable1[".add"] = true;
$tdataTable1[".afterAddAction"] = 1;
$tdataTable1[".closePopupAfterAdd"] = 1;
$tdataTable1[".afterAddActionDetTable"] = "";

$tdataTable1[".list"] = true;

$tdataTable1[".inlineEdit"] = true;
$tdataTable1[".inlineAdd"] = true;
$tdataTable1[".view"] = true;

$tdataTable1[".import"] = true;

$tdataTable1[".exportTo"] = true;

$tdataTable1[".printFriendly"] = true;

$tdataTable1[".delete"] = true;

$tdataTable1[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdataTable1[".searchSaving"] = false;
//

$tdataTable1[".showSearchPanel"] = true;
		$tdataTable1[".flexibleSearch"] = true;

if (isMobile())
	$tdataTable1[".isUseAjaxSuggest"] = false;
else
	$tdataTable1[".isUseAjaxSuggest"] = true;

$tdataTable1[".rowHighlite"] = true;



$tdataTable1[".addPageEvents"] = false;

// use timepicker for search panel
$tdataTable1[".isUseTimeForSearch"] = false;





$tdataTable1[".allSearchFields"] = array();
$tdataTable1[".filterFields"] = array();
$tdataTable1[".requiredSearchFields"] = array();

$tdataTable1[".allSearchFields"][] = "ID";
	$tdataTable1[".allSearchFields"][] = "Field1";
	

$tdataTable1[".googleLikeFields"] = array();
$tdataTable1[".googleLikeFields"][] = "ID";
$tdataTable1[".googleLikeFields"][] = "Field1";


$tdataTable1[".advSearchFields"] = array();
$tdataTable1[".advSearchFields"][] = "ID";
$tdataTable1[".advSearchFields"][] = "Field1";

$tdataTable1[".tableType"] = "list";

$tdataTable1[".printerPageOrientation"] = 0;
$tdataTable1[".nPrinterPageScale"] = 100;

$tdataTable1[".nPrinterSplitRecords"] = 40;

$tdataTable1[".nPrinterPDFSplitRecords"] = 40;



$tdataTable1[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdataTable1[".pageSize"] = 20;

$tdataTable1[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataTable1[".strOrderBy"] = $tstrOrderBy;

$tdataTable1[".orderindexes"] = array();

$tdataTable1[".sqlHead"] = "SELECT ID,  	Field1";
$tdataTable1[".sqlFrom"] = "FROM Table1";
$tdataTable1[".sqlWhereExpr"] = "";
$tdataTable1[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataTable1[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataTable1[".arrGroupsPerPage"] = $arrGPP;

$tdataTable1[".highlightSearchResults"] = true;

$tableKeysTable1 = array();
$tableKeysTable1[] = "ID";
$tdataTable1[".Keys"] = $tableKeysTable1;

$tdataTable1[".listFields"] = array();
$tdataTable1[".listFields"][] = "ID";
$tdataTable1[".listFields"][] = "Field1";

$tdataTable1[".hideMobileList"] = array();


$tdataTable1[".viewFields"] = array();
$tdataTable1[".viewFields"][] = "ID";
$tdataTable1[".viewFields"][] = "Field1";

$tdataTable1[".addFields"] = array();
$tdataTable1[".addFields"][] = "Field1";

$tdataTable1[".masterListFields"] = array();
$tdataTable1[".masterListFields"][] = "ID";
$tdataTable1[".masterListFields"][] = "Field1";

$tdataTable1[".inlineAddFields"] = array();
$tdataTable1[".inlineAddFields"][] = "Field1";

$tdataTable1[".editFields"] = array();
$tdataTable1[".editFields"][] = "Field1";

$tdataTable1[".inlineEditFields"] = array();
$tdataTable1[".inlineEditFields"][] = "Field1";

$tdataTable1[".exportFields"] = array();
$tdataTable1[".exportFields"][] = "ID";
$tdataTable1[".exportFields"][] = "Field1";

$tdataTable1[".importFields"] = array();
$tdataTable1[".importFields"][] = "ID";
$tdataTable1[".importFields"][] = "Field1";

$tdataTable1[".printFields"] = array();
$tdataTable1[".printFields"][] = "ID";
$tdataTable1[".printFields"][] = "Field1";

//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "table1";
	$fdata["Label"] = GetFieldLabel("Table1","ID");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdataTable1["ID"] = $fdata;
//	Field1
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Field1";
	$fdata["GoodName"] = "Field1";
	$fdata["ownerTable"] = "table1";
	$fdata["Label"] = GetFieldLabel("Table1","Field1");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Field1";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Field1";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdataTable1["Field1"] = $fdata;


$tables_data["Table1"]=&$tdataTable1;
$field_labels["Table1"] = &$fieldLabelsTable1;
$fieldToolTips["Table1"] = &$fieldToolTipsTable1;
$page_titles["Table1"] = &$pageTitlesTable1;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["Table1"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["Table1"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_Table1()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  	Field1";
$proto0["m_strFrom"] = "FROM Table1";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
			$proto0["cipherer"] = null;
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = false;
$proto1["m_inBrackets"] = false;
$proto1["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "table1",
	"m_srcTableName" => "Table1"
));

$proto5["m_sql"] = "ID";
$proto5["m_srcTableName"] = "Table1";
$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "Field1",
	"m_strTable" => "table1",
	"m_srcTableName" => "Table1"
));

$proto7["m_sql"] = "Field1";
$proto7["m_srcTableName"] = "Table1";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto9=array();
$proto9["m_link"] = "SQLL_MAIN";
			$proto10=array();
$proto10["m_strName"] = "table1";
$proto10["m_srcTableName"] = "Table1";
$proto10["m_columns"] = array();
$proto10["m_columns"][] = "ID";
$proto10["m_columns"][] = "Field1";
$obj = new SQLTable($proto10);

$proto9["m_table"] = $obj;
$proto9["m_sql"] = "Table1";
$proto9["m_alias"] = "";
$proto9["m_srcTableName"] = "Table1";
$proto11=array();
$proto11["m_sql"] = "";
$proto11["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto11["m_column"]=$obj;
$proto11["m_contained"] = array();
$proto11["m_strCase"] = "";
$proto11["m_havingmode"] = false;
$proto11["m_inBrackets"] = false;
$proto11["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto11);

$proto9["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto9);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="Table1";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_Table1 = createSqlQuery_Table1();



		

$tdataTable1[".sqlquery"] = $queryData_Table1;

$tableEvents["Table1"] = new eventsBase;
$tdataTable1[".hasEvents"] = false;

?>