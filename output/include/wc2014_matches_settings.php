<?php
require_once(getabspath("classes/cipherer.php"));




$tdatawc2014_matches = array();
	$tdatawc2014_matches[".truncateText"] = true;
	$tdatawc2014_matches[".NumberOfChars"] = 80;
	$tdatawc2014_matches[".ShortName"] = "wc2014_matches";
	$tdatawc2014_matches[".OwnerID"] = "";
	$tdatawc2014_matches[".OriginalTable"] = "wc2014_matches";

//	field labels
$fieldLabelswc2014_matches = array();
$fieldToolTipswc2014_matches = array();
$pageTitleswc2014_matches = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelswc2014_matches["English"] = array();
	$fieldToolTipswc2014_matches["English"] = array();
	$pageTitleswc2014_matches["English"] = array();
	$fieldLabelswc2014_matches["English"]["ID"] = "ID";
	$fieldToolTipswc2014_matches["English"]["ID"] = "";
	$fieldLabelswc2014_matches["English"]["datetime"] = "Datetime";
	$fieldToolTipswc2014_matches["English"]["datetime"] = "";
	$fieldLabelswc2014_matches["English"]["location"] = "Location";
	$fieldToolTipswc2014_matches["English"]["location"] = "";
	$fieldLabelswc2014_matches["English"]["match_number"] = "Match";
	$fieldToolTipswc2014_matches["English"]["match_number"] = "";
	$fieldLabelswc2014_matches["English"]["team1"] = "Team1";
	$fieldToolTipswc2014_matches["English"]["team1"] = "";
	$fieldLabelswc2014_matches["English"]["team2"] = "Team2";
	$fieldToolTipswc2014_matches["English"]["team2"] = "";
	$fieldLabelswc2014_matches["English"]["team1_goals"] = "";
	$fieldToolTipswc2014_matches["English"]["team1_goals"] = "";
	$fieldLabelswc2014_matches["English"]["team2_goals"] = "";
	$fieldToolTipswc2014_matches["English"]["team2_goals"] = "";
	$fieldLabelswc2014_matches["English"]["team1_code"] = "Team1 Code";
	$fieldToolTipswc2014_matches["English"]["team1_code"] = "";
	$fieldLabelswc2014_matches["English"]["team2_code"] = "Team2 Code";
	$fieldToolTipswc2014_matches["English"]["team2_code"] = "";
	$fieldLabelswc2014_matches["English"]["status"] = "Status";
	$fieldToolTipswc2014_matches["English"]["status"] = "";
	if (count($fieldToolTipswc2014_matches["English"]))
		$tdatawc2014_matches[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelswc2014_matches[""] = array();
	$fieldToolTipswc2014_matches[""] = array();
	$pageTitleswc2014_matches[""] = array();
	if (count($fieldToolTipswc2014_matches[""]))
		$tdatawc2014_matches[".isUseToolTips"] = true;
}


	$tdatawc2014_matches[".NCSearch"] = true;



$tdatawc2014_matches[".shortTableName"] = "wc2014_matches";
$tdatawc2014_matches[".nSecOptions"] = 0;
$tdatawc2014_matches[".recsPerRowList"] = 1;
$tdatawc2014_matches[".recsPerRowPrint"] = 1;
$tdatawc2014_matches[".mainTableOwnerID"] = "";
$tdatawc2014_matches[".moveNext"] = 0;
$tdatawc2014_matches[".entityType"] = 0;

$tdatawc2014_matches[".strOriginalTableName"] = "wc2014_matches";




$tdatawc2014_matches[".showAddInPopup"] = false;

$tdatawc2014_matches[".showEditInPopup"] = false;

$tdatawc2014_matches[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatawc2014_matches[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatawc2014_matches[".fieldsForRegister"] = array();

$tdatawc2014_matches[".listAjax"] = false;

	$tdatawc2014_matches[".audit"] = false;

	$tdatawc2014_matches[".locking"] = false;



$tdatawc2014_matches[".list"] = true;

$tdatawc2014_matches[".view"] = true;





$tdatawc2014_matches[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatawc2014_matches[".searchSaving"] = false;
//

$tdatawc2014_matches[".showSearchPanel"] = true;
		$tdatawc2014_matches[".flexibleSearch"] = true;

if (isMobile())
	$tdatawc2014_matches[".isUseAjaxSuggest"] = false;
else
	$tdatawc2014_matches[".isUseAjaxSuggest"] = true;

$tdatawc2014_matches[".rowHighlite"] = true;



$tdatawc2014_matches[".addPageEvents"] = true;

// use timepicker for search panel
$tdatawc2014_matches[".isUseTimeForSearch"] = false;





$tdatawc2014_matches[".allSearchFields"] = array();
$tdatawc2014_matches[".filterFields"] = array();
$tdatawc2014_matches[".requiredSearchFields"] = array();






$tdatawc2014_matches[".tableType"] = "list";

$tdatawc2014_matches[".printerPageOrientation"] = 0;
$tdatawc2014_matches[".nPrinterPageScale"] = 100;

$tdatawc2014_matches[".nPrinterSplitRecords"] = 40;

$tdatawc2014_matches[".nPrinterPDFSplitRecords"] = 40;



$tdatawc2014_matches[".geocodingEnabled"] = false;





$tdatawc2014_matches[".listGridLayout"] = 1;





// view page pdf

// print page pdf


$tdatawc2014_matches[".pageSize"] = 10;

$tdatawc2014_matches[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatawc2014_matches[".strOrderBy"] = $tstrOrderBy;

$tdatawc2014_matches[".orderindexes"] = array();

$tdatawc2014_matches[".sqlHead"] = "SELECT ID,  	team1,  	team2,  	`datetime`,  	match_number,  	location,  	team1_goals,  	team2_goals,  	team1_code,  	team2_code,  	status";
$tdatawc2014_matches[".sqlFrom"] = "FROM wc2014_matches";
$tdatawc2014_matches[".sqlWhereExpr"] = "";
$tdatawc2014_matches[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatawc2014_matches[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatawc2014_matches[".arrGroupsPerPage"] = $arrGPP;

$tdatawc2014_matches[".highlightSearchResults"] = true;

$tableKeyswc2014_matches = array();
$tableKeyswc2014_matches[] = "match_number";
$tdatawc2014_matches[".Keys"] = $tableKeyswc2014_matches;

$tdatawc2014_matches[".listFields"] = array();
$tdatawc2014_matches[".listFields"][] = "match_number";
$tdatawc2014_matches[".listFields"][] = "datetime";
$tdatawc2014_matches[".listFields"][] = "location";
$tdatawc2014_matches[".listFields"][] = "team1";
$tdatawc2014_matches[".listFields"][] = "team1_goals";
$tdatawc2014_matches[".listFields"][] = "team2_goals";
$tdatawc2014_matches[".listFields"][] = "team2";

$tdatawc2014_matches[".hideMobileList"] = array();


$tdatawc2014_matches[".viewFields"] = array();
$tdatawc2014_matches[".viewFields"][] = "match_number";
$tdatawc2014_matches[".viewFields"][] = "datetime";
$tdatawc2014_matches[".viewFields"][] = "location";
$tdatawc2014_matches[".viewFields"][] = "team1";
$tdatawc2014_matches[".viewFields"][] = "team1_goals";
$tdatawc2014_matches[".viewFields"][] = "team2_goals";
$tdatawc2014_matches[".viewFields"][] = "team2";

$tdatawc2014_matches[".addFields"] = array();

$tdatawc2014_matches[".masterListFields"] = array();
$tdatawc2014_matches[".masterListFields"][] = "ID";
$tdatawc2014_matches[".masterListFields"][] = "team2_code";
$tdatawc2014_matches[".masterListFields"][] = "team1_code";
$tdatawc2014_matches[".masterListFields"][] = "status";
$tdatawc2014_matches[".masterListFields"][] = "match_number";
$tdatawc2014_matches[".masterListFields"][] = "datetime";
$tdatawc2014_matches[".masterListFields"][] = "location";
$tdatawc2014_matches[".masterListFields"][] = "team1";
$tdatawc2014_matches[".masterListFields"][] = "team1_goals";
$tdatawc2014_matches[".masterListFields"][] = "team2_goals";
$tdatawc2014_matches[".masterListFields"][] = "team2";

$tdatawc2014_matches[".inlineAddFields"] = array();

$tdatawc2014_matches[".editFields"] = array();

$tdatawc2014_matches[".inlineEditFields"] = array();

$tdatawc2014_matches[".exportFields"] = array();

$tdatawc2014_matches[".importFields"] = array();
$tdatawc2014_matches[".importFields"][] = "ID";
$tdatawc2014_matches[".importFields"][] = "team1";
$tdatawc2014_matches[".importFields"][] = "team2";
$tdatawc2014_matches[".importFields"][] = "datetime";
$tdatawc2014_matches[".importFields"][] = "match_number";
$tdatawc2014_matches[".importFields"][] = "location";
$tdatawc2014_matches[".importFields"][] = "team1_goals";
$tdatawc2014_matches[".importFields"][] = "team2_goals";
$tdatawc2014_matches[".importFields"][] = "team1_code";
$tdatawc2014_matches[".importFields"][] = "team2_code";
$tdatawc2014_matches[".importFields"][] = "status";

$tdatawc2014_matches[".printFields"] = array();

//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "wc2014_matches";
	$fdata["Label"] = GetFieldLabel("wc2014_matches","ID");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "ID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatawc2014_matches["ID"] = $fdata;
//	team1
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "team1";
	$fdata["GoodName"] = "team1";
	$fdata["ownerTable"] = "wc2014_matches";
	$fdata["Label"] = GetFieldLabel("wc2014_matches","team1");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "team1";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "team1";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatawc2014_matches["team1"] = $fdata;
//	team2
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "team2";
	$fdata["GoodName"] = "team2";
	$fdata["ownerTable"] = "wc2014_matches";
	$fdata["Label"] = GetFieldLabel("wc2014_matches","team2");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "team2";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "team2";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatawc2014_matches["team2"] = $fdata;
//	datetime
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "datetime";
	$fdata["GoodName"] = "datetime";
	$fdata["ownerTable"] = "wc2014_matches";
	$fdata["Label"] = GetFieldLabel("wc2014_matches","datetime");
	$fdata["FieldType"] = 135;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "datetime";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`datetime`";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

		$edata["ShowTime"] = true;

	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatawc2014_matches["datetime"] = $fdata;
//	match_number
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "match_number";
	$fdata["GoodName"] = "match_number";
	$fdata["ownerTable"] = "wc2014_matches";
	$fdata["Label"] = GetFieldLabel("wc2014_matches","match_number");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "match_number";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "match_number";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatawc2014_matches["match_number"] = $fdata;
//	location
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "location";
	$fdata["GoodName"] = "location";
	$fdata["ownerTable"] = "wc2014_matches";
	$fdata["Label"] = GetFieldLabel("wc2014_matches","location");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "location";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "location";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatawc2014_matches["location"] = $fdata;
//	team1_goals
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "team1_goals";
	$fdata["GoodName"] = "team1_goals";
	$fdata["ownerTable"] = "wc2014_matches";
	$fdata["Label"] = GetFieldLabel("wc2014_matches","team1_goals");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "team1_goals";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "team1_goals";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatawc2014_matches["team1_goals"] = $fdata;
//	team2_goals
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "team2_goals";
	$fdata["GoodName"] = "team2_goals";
	$fdata["ownerTable"] = "wc2014_matches";
	$fdata["Label"] = GetFieldLabel("wc2014_matches","team2_goals");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "team2_goals";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "team2_goals";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatawc2014_matches["team2_goals"] = $fdata;
//	team1_code
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "team1_code";
	$fdata["GoodName"] = "team1_code";
	$fdata["ownerTable"] = "wc2014_matches";
	$fdata["Label"] = GetFieldLabel("wc2014_matches","team1_code");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "team1_code";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "team1_code";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatawc2014_matches["team1_code"] = $fdata;
//	team2_code
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "team2_code";
	$fdata["GoodName"] = "team2_code";
	$fdata["ownerTable"] = "wc2014_matches";
	$fdata["Label"] = GetFieldLabel("wc2014_matches","team2_code");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "team2_code";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "team2_code";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatawc2014_matches["team2_code"] = $fdata;
//	status
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "status";
	$fdata["GoodName"] = "status";
	$fdata["ownerTable"] = "wc2014_matches";
	$fdata["Label"] = GetFieldLabel("wc2014_matches","status");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "status";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "status";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatawc2014_matches["status"] = $fdata;


$tables_data["wc2014_matches"]=&$tdatawc2014_matches;
$field_labels["wc2014_matches"] = &$fieldLabelswc2014_matches;
$fieldToolTips["wc2014_matches"] = &$fieldToolTipswc2014_matches;
$page_titles["wc2014_matches"] = &$pageTitleswc2014_matches;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["wc2014_matches"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["wc2014_matches"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_wc2014_matches()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  	team1,  	team2,  	`datetime`,  	match_number,  	location,  	team1_goals,  	team2_goals,  	team1_code,  	team2_code,  	status";
$proto0["m_strFrom"] = "FROM wc2014_matches";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
			$proto0["cipherer"] = null;
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = false;
$proto1["m_inBrackets"] = false;
$proto1["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "wc2014_matches",
	"m_srcTableName" => "wc2014_matches"
));

$proto5["m_sql"] = "ID";
$proto5["m_srcTableName"] = "wc2014_matches";
$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "team1",
	"m_strTable" => "wc2014_matches",
	"m_srcTableName" => "wc2014_matches"
));

$proto7["m_sql"] = "team1";
$proto7["m_srcTableName"] = "wc2014_matches";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "team2",
	"m_strTable" => "wc2014_matches",
	"m_srcTableName" => "wc2014_matches"
));

$proto9["m_sql"] = "team2";
$proto9["m_srcTableName"] = "wc2014_matches";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto0["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "datetime",
	"m_strTable" => "wc2014_matches",
	"m_srcTableName" => "wc2014_matches"
));

$proto11["m_sql"] = "`datetime`";
$proto11["m_srcTableName"] = "wc2014_matches";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto0["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "match_number",
	"m_strTable" => "wc2014_matches",
	"m_srcTableName" => "wc2014_matches"
));

$proto13["m_sql"] = "match_number";
$proto13["m_srcTableName"] = "wc2014_matches";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto0["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "location",
	"m_strTable" => "wc2014_matches",
	"m_srcTableName" => "wc2014_matches"
));

$proto15["m_sql"] = "location";
$proto15["m_srcTableName"] = "wc2014_matches";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto0["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "team1_goals",
	"m_strTable" => "wc2014_matches",
	"m_srcTableName" => "wc2014_matches"
));

$proto17["m_sql"] = "team1_goals";
$proto17["m_srcTableName"] = "wc2014_matches";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto0["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "team2_goals",
	"m_strTable" => "wc2014_matches",
	"m_srcTableName" => "wc2014_matches"
));

$proto19["m_sql"] = "team2_goals";
$proto19["m_srcTableName"] = "wc2014_matches";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto0["m_fieldlist"][]=$obj;
						$proto21=array();
			$obj = new SQLField(array(
	"m_strName" => "team1_code",
	"m_strTable" => "wc2014_matches",
	"m_srcTableName" => "wc2014_matches"
));

$proto21["m_sql"] = "team1_code";
$proto21["m_srcTableName"] = "wc2014_matches";
$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "";
$obj = new SQLFieldListItem($proto21);

$proto0["m_fieldlist"][]=$obj;
						$proto23=array();
			$obj = new SQLField(array(
	"m_strName" => "team2_code",
	"m_strTable" => "wc2014_matches",
	"m_srcTableName" => "wc2014_matches"
));

$proto23["m_sql"] = "team2_code";
$proto23["m_srcTableName"] = "wc2014_matches";
$proto23["m_expr"]=$obj;
$proto23["m_alias"] = "";
$obj = new SQLFieldListItem($proto23);

$proto0["m_fieldlist"][]=$obj;
						$proto25=array();
			$obj = new SQLField(array(
	"m_strName" => "status",
	"m_strTable" => "wc2014_matches",
	"m_srcTableName" => "wc2014_matches"
));

$proto25["m_sql"] = "status";
$proto25["m_srcTableName"] = "wc2014_matches";
$proto25["m_expr"]=$obj;
$proto25["m_alias"] = "";
$obj = new SQLFieldListItem($proto25);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto27=array();
$proto27["m_link"] = "SQLL_MAIN";
			$proto28=array();
$proto28["m_strName"] = "wc2014_matches";
$proto28["m_srcTableName"] = "wc2014_matches";
$proto28["m_columns"] = array();
$proto28["m_columns"][] = "ID";
$proto28["m_columns"][] = "team1";
$proto28["m_columns"][] = "team2";
$proto28["m_columns"][] = "datetime";
$proto28["m_columns"][] = "match_number";
$proto28["m_columns"][] = "location";
$proto28["m_columns"][] = "team1_goals";
$proto28["m_columns"][] = "team2_goals";
$proto28["m_columns"][] = "team1_code";
$proto28["m_columns"][] = "team2_code";
$proto28["m_columns"][] = "status";
$obj = new SQLTable($proto28);

$proto27["m_table"] = $obj;
$proto27["m_sql"] = "wc2014_matches";
$proto27["m_alias"] = "";
$proto27["m_srcTableName"] = "wc2014_matches";
$proto29=array();
$proto29["m_sql"] = "";
$proto29["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto29["m_column"]=$obj;
$proto29["m_contained"] = array();
$proto29["m_strCase"] = "";
$proto29["m_havingmode"] = false;
$proto29["m_inBrackets"] = false;
$proto29["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto29);

$proto27["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto27);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="wc2014_matches";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_wc2014_matches = createSqlQuery_wc2014_matches();



											

$tdatawc2014_matches[".sqlquery"] = $queryData_wc2014_matches;

include_once(getabspath("include/wc2014_matches_events.php"));
$tableEvents["wc2014_matches"] = new eventclass_wc2014_matches;
$tdatawc2014_matches[".hasEvents"] = true;

?>